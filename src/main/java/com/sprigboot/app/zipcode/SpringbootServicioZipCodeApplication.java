package com.sprigboot.app.zipcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootServicioZipCodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioZipCodeApplication.class, args);
	}

}
