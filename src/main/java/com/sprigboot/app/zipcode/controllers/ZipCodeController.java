package com.sprigboot.app.zipcode.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sprigboot.app.zipcode.models.entity.ZipCode;
import com.sprigboot.app.zipcode.models.service.ZipCodeService;


@RestController
public class ZipCodeController {

	@Autowired
	private ZipCodeService zipCodeService;
	
	private static Logger logger= LoggerFactory.getLogger(ZipCodeService.class);

	@GetMapping("/listar")
	public List<ZipCode> listar() {
		return zipCodeService.findAll();
	}

	@GetMapping("/id/{id}")
	public ResponseEntity<?> detalle(@PathVariable("id") String id) {

		try {
			return new ResponseEntity<ZipCode>(zipCodeService.findById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ZipCode>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/zip-codes/{zipCode}")
	public ResponseEntity<?> verZipCode(@PathVariable("zipCode") String zipcode){
		try {
			return new ResponseEntity<ZipCode>(zipCodeService.findByZipCode(zipcode),HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error en verZipCode: "+e);
			return new ResponseEntity<ZipCode>(HttpStatus.NOT_FOUND);
		}
	}
}
