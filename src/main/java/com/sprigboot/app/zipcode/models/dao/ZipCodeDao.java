package com.sprigboot.app.zipcode.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.sprigboot.app.zipcode.models.entity.ZipCode;

public interface ZipCodeDao extends PagingAndSortingRepository<ZipCode, String>{
	
	@Query("select u from ZipCode u where zip_code=?1")
	public ZipCode findByZipCode(String s);
}
