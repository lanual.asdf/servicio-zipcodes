package com.sprigboot.app.zipcode.models.service;

import java.util.List;

import com.sprigboot.app.zipcode.models.entity.ZipCode;

public interface ZipCodeService {
	public List<ZipCode> findAll();
	public ZipCode findById(String id);
	public ZipCode findByZipCode(String zipcode) throws Exception;
}
