package com.sprigboot.app.zipcode.models.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "zip_code")
public class ZipCode implements Serializable {

	@Id
	@Column(unique = true, length = 5)
	private String zip_code;

	@Column(length = 30)
	private String locality;
	@Column(length = 30)
	private String federal_entity;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "zip_id")
	private List<ZipCodeDetalle> settlements;

	@Column(length = 30)
	private String municipality;

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getFederal_entity() {
		return federal_entity;
	}

	public void setFederal_entity(String federal_entity) {
		this.federal_entity = federal_entity;
	}

	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public List<ZipCodeDetalle> getSettlements() {
		return settlements;
	}

	public void setSettlements(List<ZipCodeDetalle> settlements) {
		this.settlements = settlements;
	}

	private static final long serialVersionUID = -2392815866063207193L;
}
