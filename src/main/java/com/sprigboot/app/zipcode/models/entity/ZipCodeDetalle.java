package com.sprigboot.app.zipcode.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "zip_code_detalle")
public class ZipCodeDetalle implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	private Long id;
	@Column(length = 30)
	private String name;
	@Column(length = 30)
	private String zone_type;
	@Column(length = 30)
	private String settlement_type;

	@Column(length = 5)
	private String zip_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZone_type() {
		return zone_type;
	}

	public void setZone_type(String zone_type) {
		this.zone_type = zone_type;
	}

	public String getSettlement_type() {
		return settlement_type;
	}

	public void setSettlement_type(String settlement_type) {
		this.settlement_type = settlement_type;
	}

	public String getZip_id() {
		return zip_id;
	}

	public void setZip_id(String zip_id) {
		this.zip_id = zip_id;
	}

	private static final long serialVersionUID = -9073443539828218857L;
}
