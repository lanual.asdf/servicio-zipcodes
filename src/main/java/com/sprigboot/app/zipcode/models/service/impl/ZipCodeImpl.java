package com.sprigboot.app.zipcode.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sprigboot.app.zipcode.models.dao.ZipCodeDao;
import com.sprigboot.app.zipcode.models.entity.ZipCode;
import com.sprigboot.app.zipcode.models.service.ZipCodeService;

import javassist.NotFoundException;
import net.bytebuddy.implementation.bytecode.Throw;

@Service
public class ZipCodeImpl implements ZipCodeService{
	
	@Autowired
	private ZipCodeDao zipCodeDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<ZipCode> findAll() {
		return (List<ZipCode>)zipCodeDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public ZipCode findById(String id) {
		return zipCodeDao.findById(id).orElseThrow();
	}

	@Override
	@Transactional(readOnly = true)
	public ZipCode findByZipCode(String zipcode) throws Exception {
		
		ZipCode zipCode=zipCodeDao.findByZipCode(zipcode);
		
		if(zipCode!=null)
			return zipCodeDao.findByZipCode(zipcode);
		else
			throw  new NotFoundException("ZipCode 404");
	}

}
