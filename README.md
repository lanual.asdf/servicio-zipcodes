#PROYECTO ZIP CODES MEXICO

**End Poins**

**localhost:8080/id/1110**
    
    {
        "zip_code": "1110",
        "locality": "Ciudad de México",
        "federal_entity": "Ciudad de México",
        "settlements": [
            {
                "name": "Belém de las Flores",
                "zone_type": "Urbano",
                "settlement_type": "Colonia",
                "zip_id": "1110"
            },
            {
                "name": "El Capulín",
                "zone_type": "Urbano",
                "settlement_type": "Colonia",
                "zip_id": "1110"
            },
            {
                "name": "Ampliación El Capulín",
                "zone_type": "Urbano",
                "settlement_type": "Colonia",
                "zip_id": "1110"
            },
            {
                "name": "Liberales de 1857",
                "zone_type": "Urbano",
                "settlement_type": "Colonia",
                "zip_id": "1110"
            }
        ],
        "municipality": "Álvaro Obregón"
    }
    
**localhost:8080/zip-codes/14260**
    
    {
        "zip_code": "14260",
        "locality": "Ciudad de México",
        "federal_entity": "Ciudad de México",
        "settlements": [
            {
                "name": "El Capulín",
                "zone_type": "Urbano",
                "settlement_type": "Barrio",
                "zip_id": "14260"
            },
            {
                "name": "Miguel Hidalgo 1A Sección",
                "zone_type": "Urbano",
                "settlement_type": "Colonia",
                "zip_id": "14260"
            }
        ],
        "municipality": "Tlalpan"
    }

**localhost:8080/listar**
    Muestra Todo los Codigos postales para todas las ciudadades y municipios
    
    
**Tecnologia utilizada**
:: Spring Boot      ::  (v2.2.6.RELEASE)
:: JDK              ::  11.06
:: Spring Data JPA  ::    
:: MYSQL            ::  